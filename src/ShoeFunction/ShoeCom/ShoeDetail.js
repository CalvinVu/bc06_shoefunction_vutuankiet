import React from "react";
import { Card } from "antd";
const { Meta } = Card;

export default function ShoeDetail({ shoeDetail }) {
  return (
    <div>
      <Card
        hoverable
        style={{ width: 240 }}
        cover={<img alt="example" src={shoeDetail.image} />}
      >
        <Meta title={shoeDetail.name} description={shoeDetail.description} />
      </Card>
    </div>
  );
}
