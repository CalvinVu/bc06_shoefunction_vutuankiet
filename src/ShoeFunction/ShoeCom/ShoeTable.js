import React from "react";
import { Table } from "antd";

export default function ShoeTable({ shoeAdd, handleCount, handleDelete }) {
  const data = shoeAdd;
  const columns = [
    {
      title: "Name",
      dataIndex: "name",
      key: "name",
    },
    {
      title: "Price",
      dataIndex: "price",
      key: "age",
    },
    {
      title: "Image",
      dataIndex: "image",
      render: (image) => <img src={image} style={{ width: "50px" }} />,
    },
    {
      title: "Quantity",
      dataIndex: "soLuong",
      key: "soLuong",
      render: (soLuong, id) => {
        return (
          <div>
            <button
              className="btn btn-dark"
              onClick={() => {
                handleCount(id, -1);
              }}
            >
              -
            </button>
            <strong>{soLuong}</strong>
            <button
              className="btn btn-secondary"
              onClick={() => {
                handleCount(id, +1);
              }}
            >
              +
            </button>
          </div>
        );
      },
    },
    {
      title: "Action",
      dataIndex: "",
      key: "x",
      render: (id) => (
        <button
          onClick={() => {
            handleDelete(id);
          }}
        >
          Delete
        </button>
      ),
    },
  ];

  return (
    <div>
      <Table
        columns={columns}
        expandable={{
          expandedRowRender: (record) => (
            <p
              style={{
                margin: 0,
              }}
            >
              {record.description}
            </p>
          ),
          rowExpandable: (record) => record.name !== "Not Expandable",
        }}
        dataSource={data}
      />
    </div>
  );
}
