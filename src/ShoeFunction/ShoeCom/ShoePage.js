import React, { useState } from "react";
import { data } from "../DataJS/shoeData";
import { Avatar, Card } from "antd";
import ShoeDetail from "./ShoeDetail";
import ShoeTable from "./ShoeTable";
const { Meta } = Card;

export default function ShoePage() {
  const [shoeArr, setShoeArr] = useState(data);
  const [shoeDetail, setShoeDetail] = useState(data[0]);
  const [shoeAdd, setShoeAdd] = useState([]);

  let handleShoeDetail = (index) => {
    setShoeDetail(data[index]);
  };
  let newAdd = [...shoeAdd];

  let handleShoeAdd = (item) => {
    let index = newAdd.findIndex((shoe) => {
      return shoe.id == item.id;
    });
    console.log(index);
    if (newAdd == "") {
      newAdd.push(item);
      item.soLuong = 1;
    } else if (index == -1) {
      newAdd.push(item);
      item.soLuong = 1;
    } else {
      newAdd[index].soLuong++;
    }

    setShoeAdd(newAdd);
  };

  let handleCount = (id, select) => {
    let index = newAdd.findIndex((item) => {
      return item.id == id.id;
    });
    newAdd[index].soLuong = newAdd[index].soLuong + select;
    newAdd[index].soLuong == 0 && newAdd.splice(index, 1);
    setShoeAdd(newAdd);
  };

  let handleDelete = (id) => {
    let index = newAdd.findIndex((item) => {
      return item.id == id.id;
    });
    newAdd.splice(index, 1);
    setShoeAdd(newAdd);
  };

  let renderShoeArr = () => {
    return shoeArr.map((item) => {
      return (
        <Card
          style={{
            width: 300,
          }}
          className="col-6"
          cover={
            <img
              alt="example"
              src={item.image}
              style={{ width: "200px", margin: "auto" }}
            />
          }
          actions={[]}
        >
          <div className="pb-5">
            <button
              className="btn btn-warning"
              onClick={() => {
                handleShoeDetail(item.id);
              }}
            >
              DETAIL
            </button>
            <button
              className="btn btn-dark"
              onClick={() => {
                handleShoeAdd(item);
              }}
            >
              ADD
            </button>
          </div>
          <Meta
            avatar={<Avatar src={item.image} />}
            title={item.name}
            description={item.price}
          ></Meta>
        </Card>
      );
    });
  };
  return (
    <div className="row">
      <div className="row col-4">{renderShoeArr()}</div>
      <div className="col-3" style={{ position: "fixed", right: "0px" }}>
        <ShoeDetail shoeDetail={shoeDetail} />
      </div>
      <div style={{}}>
        <ShoeTable
          shoeAdd={shoeAdd}
          handleCount={handleCount}
          handleDelete={handleDelete}
        />
      </div>
    </div>
  );
}
