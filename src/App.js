import logo from "./logo.svg";
import "./App.css";
import ShoePage from "./ShoeFunction/ShoeCom/ShoePage";

function App() {
  return (
    <div className="App">
      <ShoePage />
    </div>
  );
}

export default App;
